const Generator = require("yeoman-generator");
const yosay = require("yosay");

module.exports = class extends Generator {
  async prompting() {
    this.log(yosay(`Hallo! Lets generater a opinated react boilerplate :-)`));

    const answers = await this.prompt([
      {
        name: "projectName",
        type: "input",
        message: "Project name:",
        default: this.appname
      },
      {
        name: "projectDescription",
        type: "input",
        message: "Project description:"
      },
      {
        name: "projectVersion",
        type: "input",
        message: "Project version:",
        default: "0.1.0"
      },
      {
        name: "authorName",
        type: "input",
        message: "Author name:",
        default: "Holygrow"
      }
    ]);

    this.projectName = answers.projectName;
    this.projectDescription = answers.projectDescription;
    this.projectVersion = answers.projectVersion;
    this.authorName = answers.authorName;
  }

  copyPackageJSON() {
    this.fs.copyTpl(
      this.templatePath("package.json"),
      this.destinationPath("package.json"),
      {
        projectName: this.projectName,
        projectDescription: this.projectDescription,
        projectVersion: this.projectVersion,
        authorName: this.authorName
      }
    );
  }

  copyRoot() {
    const rootFiles = [
      "editorconfig",
      "eslintignore",
      "eslintrc.json",
      "gitattributes",
      "gitignore",
      "prettierrc",
      "env.local.example",
      "env.production"
    ];

    rootFiles.forEach(file => {
      this.fs.copy(this.templatePath(file), this.destinationPath(`.${file}`));
    });
  }

  copyPublic() {
    this.fs.copy(this.templatePath("public"), this.destinationPath("public"));
  }

  copySrc() {
    this.fs.copy(this.templatePath("src"), this.destinationPath("src"));
  }

  installPackages() {
    this.npmInstall([
      "@emotion/core",
      "@emotion/styled",
      "prop-types",
      "react",
      "react-dom",
      "react-redux",
      "react-router-dom",
      "react-scripts",
      "react-redux",
      "redux-thunk",
      "react-router-dom",
      "redux-devtools-extension"
    ]);

    this.npmInstall(
      [
        "eslint-config-airbnb",
        "eslint-config-prettier",
        "eslint-plugin-import",
        "eslint-plugin-jsx-a11y",
        "eslint-plugin-prettier",
        "eslint-plugin-react",
        "husky",
        "prettier",
        "pretty-quick"
      ],
      { "save-dev": true }
    );
  }
};
